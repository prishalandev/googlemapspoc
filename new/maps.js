/**
 *  TODO:
 *      - Disable textbox, and enable only when gmap services loaded correctly
 *      x convert 'get Place Details' anto a promise
 *      - override checkbox
 *      - override marker
 *      - place details info-panel
 *      - map PlacesDetails-FormmattedAddress object to custom object
 */

const txtAddressSearch  = document.getElementById('locAddressSearch'),
    ulPredictions       = document.getElementById('resultsAddressSearch'),
    placeDetails        = document.getElementById('place-details');

let svcMapAutocomplete,
    svcMapPlaces,
    filteredTown = {},
    map,
    markerOriginal,
    markerOverride,
    infoWindowOrignal,
    infoWindowOverride;



function initMapService() {
    svcMapAutocomplete = new google.maps.places.AutocompleteService();
    svcMapPlaces = new google.maps.places.PlacesService(placeDetails);
    map = null;
    markerOriginal = null;
    markerOverride = null;
    infoWindowOrignal = null;
    infoWindowOverride = null;

    txtAddressSearch.addEventListener('keyup', function(event) {
        if (event.which !== 8 || event.which !== 46) {
            if (txtAddressSearch.value.length < mapConfig.autocomplete.minLength) {
                changeAutocompleteStatusText(mapConfig.autocomplete.minLength - txtAddressSearch.value.length + ' character(s) left');
            }
        }
        let ignoreKeys = [40, 38, 37, 39, 8, 9, 13, 16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 45, 46, 144, 145, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 144, 145];
        if (ignoreKeys.indexOf(event.which) === -1) {
            if (txtAddressSearch.value.length >= mapConfig.autocomplete.minLength) {
                changeAutocompleteStatusText('Searching...');
                ulPredictions.innerHTML = '';
                ulPredictions.classList.remove('active');
                getSearchPredictionsPromise().then((results) => {
                    console.log(results); // TODO: remove
                    changeAutocompleteStatusText('');
                    results.forEach((result) => {
                        var li = document.createElement('li');
                        li.innerHTML = result.description;
                        li.setAttribute('data-placeid', result.place_id);
                        ulPredictions.appendChild(li);
                    });
                    togglePredictionsDropdown(true);
                    initSearchResultsKeyboardNavigation();
                }, (error) => {
                    switch (error.message) {
                        case 'ZERO_RESULTS':
                            changeAutocompleteStatusText('No results found');
                            break;
                        default:
                            break;
                    }
                    console.log(error); //  TODO: potentially remove
                });
            }
        }
    });
}



function getSearchPredictionsPromise() {
    let options = {
        input: txtAddressSearch.value,
        types: mapConfig.predictions.returnTypes,
        componentRestrictions: { country: 'za' },
        // location: new google.maps.LatLng(townLatLng.lat, townLatLng.lng),
        // radius: mapConfig.filter.radius,
    };

    return new Promise((resolve, reject) => {
        svcMapAutocomplete.getPlacePredictions(options, (predictions, status) => {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                reject(new Error(status));
            }
            else {
                resolve(predictions);
            }
        });
    });
}


function initSearchResultsKeyboardNavigation() {
    let predictionListItems = ulPredictions.getElementsByTagName('li');
    // console.log(predictionListItems); // TODO: remove
    let selectedPrediction = null;
    txtAddressSearch.addEventListener('keydown', (event) => {
        // console.log(predictionListItems.length); // TODO: remove
        // console.log(selectedPrediction); // TODO: remove

        switch (event.which) {
            case 40: // down-arrow key
                if (selectedPrediction) {
                    selectedPrediction.classList.remove('active');
                    nextPrediction = selectedPrediction.nextSibling;
                    if (nextPrediction !== null) {
                        selectedPrediction = nextPrediction;
                        selectedPrediction.classList.add('active');
                    }
                    else {
                        selectedPrediction = predictionListItems.item(0);
                        selectedPrediction.classList.add('active');
                    }
                    // activatePredictionByArrowKeys(predictionListItems, selectedPrediction, true);
                }
                else {
                    selectedPrediction = predictionListItems.item(0);
                    selectedPrediction.classList.add('active');
                }
                break;
            case 38: // up-arrow key
                if (selectedPrediction) {
                    selectedPrediction.classList.remove('active');
                    nextPrediction = selectedPrediction.previousSibling;
                    if (nextPrediction !== null) {
                        selectedPrediction = nextPrediction;
                        selectedPrediction.classList.add('active');
                    }
                    else {
                        selectedPrediction = predictionListItems.item(predictionListItems.length - 1);
                        selectedPrediction.classList.add('active');
                    }
                }
                else {
                    selectedPrediction = predictionListItems.item(predictionListItems.length - 1);
                    selectedPrediction.classList.add('active');
                }
                break;
            case 13: // enter key
                if (selectedPrediction) {
                    txtAddressSearch.value = selectedPrediction.innerHTML;
                    togglePredictionsDropdown(false);
                    showPlaceonMap(selectedPrediction.getAttribute('data-placeid'));
                }
                break;
            default:
                break;
        }
    });
}


function showPlaceonMap(placeId) {
    changeAutocompleteStatusText('Resolving address on map...');
    let detailsPromise = new Promise((resolve, reject) => {
        svcMapPlaces.getDetails({
            placeId: placeId,
            fields: mapConfig.autocomplete.requiredFields
        }, (place, status) => {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                reject(new Error(status));
            }
            else {
                resolve(place);
            }
        });
    });

    detailsPromise.then(
        (place) => {
            changeAutocompleteStatusText('');
            let selectedPlaceLatLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: selectedPlaceLatLng,
                disableDefaultUI: false,
                streetViewControl: false,
                scaleControl: true,
                rotateControl: false,
                mapTypeControl: false,
                gestureHandling: 'cooperative',
                styles: mapConfig.mapStyling,
            });
            markerOriginal = new google.maps.Marker({
                position: selectedPlaceLatLng,
                map: map,
                draggable: false,
                label: 'A',
                animation: google.maps.Animation.DROP,
            });
            let infoWindowOriginalContent = '<div class="iw-content">' +
                '<h6>Selected Address:</h6>' +
                '<p>' + place.formatted_address + '</p>' +
                '</div>';
            infoWindowOrignal = new google.maps.InfoWindow({
                content: infoWindowOriginalContent,
                maxWidth: 200
            });
            setTimeout(() => {
                openMarkerInfoWindow(infoWindowOrignal, markerOriginal);
            }, 750);            

            markerOriginal.addListener('click', () => {
                openMarkerInfoWindow(infoWindowOrignal, markerOriginal);
            });

        },
        (error) => {
            console.log(error);
        }
    );
}


// ---------------------- UTILITY FUNCTIONS -------------------------------------------------------
//  TODO: Fix this function ...
function activatePredictionByArrowKeys(predictionListItems, activeElem, isDirectionDown) {
    activeElem.classList.remove('active');
    nextElem = (isDirectionDown) ? activeElem.nextSibling : activeElem.previousSibling;
    if (nextElem !== null) {
        activeElem = nextElem;
        activeElem.classList.add('active');
    }
    else {
        activeElem = (isDirectionDown) ? predictionListItems.item(0) : predictionListItems.item(predictionListItems.length - 1);
        activeElem.classList.add('active');
    }
    return activeElem;
}


function changeAutocompleteStatusText(value) {
    document.getElementById('statusAddressSearch').innerHTML = value;
}


function togglePredictionsDropdown(show) {
    if (show) {
        ulPredictions.classList.add('active');
    }
    else {
        ulPredictions.classList.remove('active');
    }
}


function openMarkerInfoWindow(whichInfoWindow, whichMarker) {
    if (whichInfoWindow && whichMarker) {
        whichInfoWindow.open(map, whichMarker);
    }
}


/**
 * Credit: https://www.barattalo.it/coding/decimal-degrees-conversion-and-distance-of-two-points-on-google-map/
 */
function getDistanceBetweenMarkers(lat1,lon1,lat2,lon2) {
    let R = 6371;
    let dLat = (lat2-lat1) * Math.PI / 180;
    let dLon = (lon2-lon1) * Math.PI / 180; 
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) * 
        Math.sin(dLon/2) * Math.sin(dLon/2); 
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    let d = R * c;
    if (d > 1) return Math.round(d)+"km";
    else if (d<=1) return Math.round(d*1000)+"m";
    return d;
}


/**
 * Credit: https://www.geodatasource.com/developers/javascript
 */
function distance(lat1, lon1, lat2, lon2, unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist;
	}
}