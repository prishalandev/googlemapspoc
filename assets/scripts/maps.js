const   txtFilterArea = document.getElementById('locFilter'),
        txtAutocomplete = document.getElementById('locSearch');

let areasData,
    map,
    autocompleteService,
    autocompleteStatus,
    autocompletePredictions,
    filterCoords = {};


function initAreaFilter() {    
    fetch("/data/area-geodata.json").then(
        (response) => response.json()
    ).then(function(data) {
        areasData = data;
        new Awesomplete(txtFilterArea,{
            list: areasData,
            maxItems: 15,
            data: function (item, input) {
                return { label: item.t, value: item.t};
            },
        });
    }).catch(function() {
        alert('Could not load Area Geo-Data!');
    });

    txtFilterArea.addEventListener('awesomplete-selectcomplete', function(event) {
        var selectedArea = areasData.filter(function(data) {
            return data.t == event.text.value;
        });
        filterCoords.lat = selectedArea[0].la;
        filterCoords.lng = selectedArea[0].lg;
        document.getElementById('locSearch').removeAttribute('disabled');
        document.getElementById('locSearch').focus();

        console.log(filterCoords.lat + ',' + filterCoords.lng); // <<<<<<< TODO: REMOVE
    });
    
    txtFilterArea.addEventListener('keyup', function(event) {
        if (event.target.value.length < 1) {
            document.getElementById('locSearch').setAttribute('disabled', 'disabled');
        }
    });


}


function initMapService() {
    // autocompleteService = new google.maps.places.AutocompleteService();
    // autocompleteService.getPlacePredictions(

    // );

    txtAutocomplete.addEventListener('keyup', (event) => {
        if (txtAutocomplete.value.length >= 7) {
            let svc = new google.maps.places.AutocompleteService();
            var options = {
                input: txtAutocomplete.value,
                types: ['(geocode)'],
                componentRestrictions: {country: 'za'}
            };
            svc.getPlacePredictions(options , function (predictions, status) {
                if (status != google.maps.places.PlacesServiceStatus.OK) {
                    return;
                }
                prdArr = [];
                predictions.forEach(function (prediction) {
                    prdArr.push(prediction);
                });
                console.dir(prdArr);
            });
        }
    });
}


function closeMapOverlay() {
    document.getElementById('map-overlay').classList.remove('active');
}









function initAutocomplete() {
    const googleComponents = [
      { googleComponent: 'sublocality_level_1', id: 'city-address-field' },
      { googleComponent: 'locality', id: 'city-address-field' },
      { googleComponent: 'administrative_area_level_1', id: 'state-address-field' },
      { googleComponent: 'postal_code', id: 'postal-code-address-field' },
    ];
    const autocompleteFormField = document.getElementById('locSearch');
    
    this.initGooglePlacesAutocomplete(autocompleteFormField);
}