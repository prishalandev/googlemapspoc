const mapConfig = {
    api: {
        key:        'AIzaSyDDhQmSuJYvvn0H45Wy7DA7lZLa-rg70ww',
        callback:   'initMapService'
    },
    filter: {
        radius:     5000
    },
    autocomplete: {
        minLength: 7
    }
};


const mapStyles = [
    {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    },
    {
        "featureType": "poi.business",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    }
];