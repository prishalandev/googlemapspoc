const txtTownFilter       = document.getElementById('locTownFilter'),
    txtAddressSearch      = document.getElementById('locAddressSearch'),
    ulPredictions         = document.getElementById('predictions'),
    lblAutocompleteStatus = document.getElementById('predictionsautocomplete-status');


let townData,
    townLatLng = {},
    searchResults = [],
    selectedSearchResult; 


function initTownFilter() {
    fetch("/data/area-geodata.json").then(
        (response) => response.json()
    ).then(function(data) {
        townData = data;
        new Awesomplete(txtTownFilter,{
            list: townData,
            maxItems: 15,
            data: function (item, input) {
                return { label: item.t, value: item.t};
            },
        });
        txtTownFilter.removeAttribute('disabled');
        txtTownFilter.focus();    
    }).catch(function() {
        alert('Could not load Area Geo-Data!');
    });

    txtTownFilter.addEventListener('awesomplete-selectcomplete', function(event) {
        let selectedTown = townData.filter((data) => {
            return data.t == event.text.value;
        });
        townLatLng.lat = selectedTown[0].la;
        townLatLng.lng = selectedTown[0].lg;
        // txtAddressSearch.removeAttribute('disabled');
        toggleAddressSearchTextboxState(false);
        txtAddressSearch.focus();
    });
    
    txtTownFilter.addEventListener('keyup', function(event) {
        if (event.target.value.length < 1) {
            // txtAddressSearch.setAttribute('disabled', 'disabled');
            toggleAddressSearchTextboxState(true);
            townLatLng = {};
        }
    });
}



function initMapService() {
    txtAddressSearch.addEventListener('keyup', (event) => {
        let ignoreKeys = [40, 38, 37, 39, 8, 9, 13, 16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 45, 46, 144, 145, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 144, 145];
        if (ignoreKeys.indexOf(event.which) === -1) {
            if (txtAddressSearch.value.length >= mapConfig.autocomplete.minLength) {
                searchForAddresses(event);
            }
        }        
    });
}



function initAddressSearchKeyboardEvents() {
    let predictionItems = null,
        predictionSelected = null,
        predictionIndexLength = 0,
        predictionIndex = -1;

    // .... ajax request here to populate predictions
    predictionItems = ulPredictions.getElementsByTagName('li');
    predictionIndexLength = predictionItems.length - 1;

    txtAddressSearch.addEventListener('keydown', (event) => {
        if (predictionItems.length >= 1) {
            switch (event.which) {
                case 40: // Arrow-Down key
                    predictionIndex++;    
                    if (predictionSelected) {
                        clearActiveClassFromPredictions();
                        // predictionSelected.classList.remove('active');
                        predictionNextItem = predictionItems[predictionIndex];
                        if (typeof(predictionNextItem) !== null && predictionIndex <= predictionIndexLength) {
                            predictionSelected = predictionNextItem;
                        }
                        else {
                            predictionIndex = 0;
                            predictionSelected = predictionItems[predictionIndex];
                        }
                        predictionSelected.classList.add('active');
                    }
                    else {
                        predictionSelected = predictionItems[predictionIndex];
                        predictionSelected.classList.add('active');
                    }
                    break;
                case 38: // Arrow-Up key
                    if (predictionSelected) {
                        predictionIndex--;
                        clearActiveClassFromPredictions();
                        // predictionSelected.classList.remove('active');
                        predictionNextItem = predictionItems[predictionIndex];
                        if (typeof(predictionNextItem) !== null && predictionIndex >= 0) {
                            predictionSelected = predictionNextItem;
                        }
                        else {
                            predictionIndex = predictionIndexLength;
                            predictionSelected = predictionItems[predictionIndex];
                        }
                        predictionSelected.classList.add('active');
                    }
                    else {
                        predictionIndex = 0;
                        predictionSelected = predictionItems[predictionIndexLength];
                        predictionSelected.classList.add('active');
                    }
                    break;
                case 13: // Enter key
                    if (typeof(predictionNextItem) !== null && predictionIndex >= 0) {
                        txtAddressSearch.value = predictionSelected.innerHTML;
                    }
                    break;
                default:
                    break;
            }
        }
    });
}



function searchForAddresses() {
    var displaySuggestions = function(predictions, status) {
        if (status != google.maps.places.PlacesServiceStatus.OK) {
            console.log(status);
            //TODO: handle ZERO_RESULTS
            return;
        }

        ulPredictions.innerHTML = '';
        
        // var arrPr = []; 
        searchResults = [];
        predictions.forEach(function (prediction) {
            // arrPr.push(getPredictionDetails(prediction.place_id));
            searchResults.push(getPredictionDetails(prediction.place_id));
        });
        
        // Promise.all(arrPr).then(function(results) {
        Promise.all(searchResults).then(function(results) {
            results.forEach(function(result, i) {
                // console.dir(result); //TODO: Remove
                var li = document.createElement('li');
                li.innerHTML = result.formatted_address;
                li.setAttribute('data-placeid', result.place_id);
                li.setAttribute('data-index', i);
                ulPredictions.appendChild(li);
            });
            console.dir(searchResults);
            initAddressSearchKeyboardEvents();
        }).catch(err => {
            console.log(err);
            //TODO: handle OVER_QUERY_LIMIT
        });
    };
    
    function getPredictionDetails(placeId) {
        let promise = new Promise( (resolve, reject) => {
                    placeService.getDetails({
                placeId: placeId,
                fields: ["address_component", "formatted_address", "geometry.location", "place_id"]
            }, function(result, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        resolve(result);
                } else {
                        reject(status);
                }
            });        
        });
        
        return promise;
    }

    let query = txtAddressSearch.value;
    let restrictions = {country: 'za'};
    let service = new google.maps.places.AutocompleteService();
    let placeService = new google.maps.places.PlacesService(ulPredictions);
    service.getPlacePredictions({ 
        input: query, 
        types: ['geocode'], 
        componentRestrictions: restrictions,
        location: new google.maps.LatLng(townLatLng.lat, townLatLng.lng),
        radius: mapConfig.filter.radius,
    },  displaySuggestions);
}



// ================ SUPPORTING UTLITY FUNCTIONS
function clearActiveClassFromPredictions() {
    let listItems = ulPredictions.getElementsByTagName('li');
    for (let i = 0; i < listItems.length; i++) {
        listItems[i].classList.remove('active');
    }
}


function toggleAddressSearchTextboxState(disabled) {
    if (disabled) {
        txtAddressSearch.setAttribute('disabled', 'disabled');
        txtAddressSearch.setAttribute('placeholder','');
    } else {
        txtAddressSearch.removeAttribute('disabled');
        txtAddressSearch.setAttribute('placeholder','Start typing the Customer\'s address');
    }
}