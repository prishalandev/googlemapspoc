const
    mapOverlayContainer     = document.getElementById('map-overlay');
    txtTownFilter           = document.getElementById('map-filter-input'),              // mapFilterInput
    txtAddressSearch        = document.getElementById('map-address-search-input'),      // mapAddressSearchInput
    ulPredictions           = document.getElementById('map-address-search-results'),    // mapAddressSearchResults
    addressSearchStatus     = document.getElementById('map-address-search-status'),     // mapAddressSearchStatus
    placeDetails            = document.getElementById('map-place-details-service'),             // mapPlaceDetails
    chkOverrideMarker       = document.getElementById('map-override-marker'),
    lblOverrideDistance     = document.getElementById('map-place-details-override'),
    btnAddressAccept        = document.getElementById('map-accept');

let
    svcMapAutocomplete,     // serviceMapAutocomplete
    svcMapPlaces,           // serviceMapPlaces
    filteredTown = {},      // filteredTownGeometry
    map,
    markerOriginal,
    markerOverride,
    infoWindowOrignal,
    infoWindowOverride,
    selectedPlaceLatLng,
    rulerpoly;


// ==================== INITIALIZER METHODS =======================================================
function initTownFilter() {
    let townsData;
    fetch("./data/area-geodata.json").then(
        (response) => response.json()
    ).then(function(data) {
        townsData = data;
        new Awesomplete(txtTownFilter,{
            list: townsData,
            maxItems: 15,
            data: function (item, input) {
                return { label: item.t, value: item.t};
            },
        });
        txtTownFilter.removeAttribute('disabled');
        txtTownFilter.focus();    
    }).catch(function() {
        alert('Could not load Area Geo-Data!');
    });

    txtTownFilter.addEventListener('awesomplete-selectcomplete', function(event) {
        let selectedTown = townsData.filter((data) => {
            return data.t == event.text.value;
        });
        filteredTown.lat = selectedTown[0].la;
        filteredTown.lng = selectedTown[0].lg;
        toggleAddressSearchTextboxState(false);
        toggleAddressAvailableForSelection(false);
        selectedPlaceLatLng = null;
    });
    
    txtTownFilter.addEventListener('keyup', function(event) {
        if (event.target.value.length < 1) {
            toggleAddressSearchTextboxState(true);
            townLatLng = {};
        }
    });
}


function initMapService() {
    toggleElementVisibility(mapOverlayContainer, true);
    txtTownFilter.focus();

    svcMapAutocomplete  = new google.maps.places.AutocompleteService();
    svcMapPlaces        = new google.maps.places.PlacesService(placeDetails);
    map                 = null;
    markerOriginal      = null;
    markerOverride      = null;
    infoWindowOrignal   = null;
    infoWindowOverride  = null;
    selectedPlaceLatLng = null;
    rulerpoly           = null;

    let timeout = null;

    txtAddressSearch.addEventListener('keyup', function(event) {
        if (event.which !== 8 || event.which !== 46) {
            if (txtAddressSearch.value.length < mapConfig.autocomplete.minLength) {
                changeAutocompleteStatusText(mapConfig.autocomplete.minLength - txtAddressSearch.value.length + ' character(s) left');
            }
        }

        clearTimeout(timeout);

        // purposeful delay of the prediction results to avoid potential duplicate results due to "fast typing"
        timeout = setTimeout(() => {            
            // ignore keys that are not textual inputs -- https://css-tricks.com/snippets/javascript/javascript-keycodes/
            let ignoreKeys = [40, 38, 37, 39, 8, 9, 13, 16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 45, 46, 144, 145, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 144, 145];
            if (ignoreKeys.indexOf(event.which) === -1) {
                if (txtAddressSearch.value.length >= mapConfig.autocomplete.minLength) {
                    changeAutocompleteStatusText('Searching...');
                    ulPredictions.innerHTML = '';
                    toggleElementVisibility(ulPredictions, false);
                    getSearchPredictionsPromise().then((results) => {
                        if (mapConfig.log.predictionResults) {
                            console.log(results);
                        }
                        changeAutocompleteStatusText('');
                        results.forEach((result) => {
                            var li = document.createElement('li');
                            li.innerHTML = result.description;
                            li.setAttribute('data-placeid', result.place_id);
                            ulPredictions.appendChild(li);
                        });
                        toggleElementVisibility(ulPredictions, true);
                        activateSearchResultsMouseSelection();
                        activateSearchResultsKeyboardNavigation();
                    }, (error) => {
                        switch (error.message) {
                            case 'ZERO_RESULTS':
                                changeAutocompleteStatusText('No results found');
                                break;
                            default:
                                break;
                        }
                        if (mapConfig.log.predictionErrors) {
                            console.log(error);
                        }
                    });
                }
            }
        }, mapConfig.predictions.keydownDelay);
    });
}



// ==================== ANCILLIARY METHODS ========================================================
function getSearchPredictionsPromise() {
    let options = {
        input: txtAddressSearch.value,
        types: mapConfig.predictions.returnTypes,
        componentRestrictions: { country: 'za' },
        location: new google.maps.LatLng(filteredTown.lat, filteredTown.lng),
        radius: mapConfig.filter.radius,
    };

    return new Promise((resolve, reject) => {
        svcMapAutocomplete.getPlacePredictions(options, (predictions, status) => {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                reject(new Error(status));
            }
            else {
                resolve(predictions);
            }
        });
    });
}

function activateSearchResultsMouseSelection() {
    ulPredictions.addEventListener('click', (e) => {
        txtAddressSearch.value = e.target.innerHTML;
        showPlaceonMap(e.target.getAttribute('data-placeid'));
        toggleElementVisibility(ulPredictions, false);
    });
}


function activateSearchResultsKeyboardNavigation() {
    let predictionListItems = ulPredictions.getElementsByTagName('li');
    let selectedPrediction = null;
    txtAddressSearch.addEventListener('keydown', (event) => {
        switch (event.which) {
            case 40: // down-arrow key
                if (selectedPrediction) {
                    selectedPrediction.classList.remove('active');
                    nextPrediction = selectedPrediction.nextSibling;
                    if (nextPrediction !== null) {
                        selectedPrediction = nextPrediction;
                        selectedPrediction.classList.add('active');
                    }
                    else {
                        selectedPrediction = predictionListItems.item(0);
                        selectedPrediction.classList.add('active');
                    }
                }
                else {
                    selectedPrediction = predictionListItems.item(0);
                    selectedPrediction.classList.add('active');
                }
                break;
            case 38: // up-arrow key
                if (selectedPrediction) {
                    selectedPrediction.classList.remove('active');
                    nextPrediction = selectedPrediction.previousSibling;
                    if (nextPrediction !== null) {
                        selectedPrediction = nextPrediction;
                        selectedPrediction.classList.add('active');
                    }
                    else {
                        selectedPrediction = predictionListItems.item(predictionListItems.length - 1);
                        selectedPrediction.classList.add('active');
                    }
                }
                else {
                    selectedPrediction = predictionListItems.item(predictionListItems.length - 1);
                    selectedPrediction.classList.add('active');
                }
                break;
            case 13: // enter key
                if (selectedPrediction) {
                    txtAddressSearch.value = selectedPrediction.innerHTML;
                    toggleElementVisibility(ulPredictions, false);
                    // google.maps.event.clearInstanceListeners(markerOverride);
                    showPlaceonMap(selectedPrediction.getAttribute('data-placeid'));
                }
                break;
            default:
                break;
        }
    });
}

function showPlaceonMap(placeId) {
    changeAutocompleteStatusText('Resolving address on map...');
    let detailsPromise = new Promise((resolve, reject) => {
        svcMapPlaces.getDetails({
            placeId: placeId,
            fields: mapConfig.autocomplete.requiredFields
        }, (place, status) => {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                reject(new Error(status));
            }
            else {
                resolve(place);
            }
        });
    });

    detailsPromise.then(
        (place) => {
            changeAutocompleteStatusText('');
            toggleAddressAvailableForSelection(true);
            chkOverrideMarker.checked = false;
            markerOverride = null;
            infoWindowOverride = null;
            selectedPlaceLatLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            
            if (mapConfig.log.selectedAddressComponents) {
                console.log(place.address_components);
            }
            
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: selectedPlaceLatLng,
                disableDefaultUI: false,
                streetViewControl: false,
                scaleControl: true,
                rotateControl: false,
                mapTypeControl: false,
                gestureHandling: 'cooperative',
                styles: mapConfig.mapStyling,
            });
            markerOriginal = new google.maps.Marker({
                position: selectedPlaceLatLng,
                map: map,
                draggable: false,
                label: 'A',
                animation: google.maps.Animation.DROP,
            });
            let infoWindowOriginalContent = '<div class="iw-content">' +
                '<h6>Selected Address:</h6>' +
                '<p>' + place.formatted_address + '</p>' +
                '</div>';
            infoWindowOrignal = new google.maps.InfoWindow({
                content: infoWindowOriginalContent,
                maxWidth: 200
            });
            setTimeout(() => {
                openMarkerInfoWindow(infoWindowOrignal, markerOriginal);
            }, 750);            

            markerOriginal.addListener('click', () => {
                openMarkerInfoWindow(infoWindowOrignal, markerOriginal);
            });

            mapAndReturnAddressData(place);
        },
        (error) => {
            if (mapConfig.log.selectedAddressError) {
                console.log(error);
            }
        }
    );
}


function overrideDeliveryPoint() {
    if (chkOverrideMarker.checked) {
        let distanceFromOrigin = '';
        infoWindowOrignal.close();

        markerOverride = new google.maps.Marker({
            position: selectedPlaceLatLng,
            map: map,
            draggable: true,
            label: 'OV',
            title: 'Drag to an alternate Delivery Point',
        });

        infoWindowOverride = new google.maps.InfoWindow({
            content: '<div class="iw-content">' +
            '<h6>Delivery Point Override:</h6>' +
            '<table>' +
            '<tr><td>Latitude:</td><td>' + markerOverride.getPosition().lat() + '</td></tr>' +
            '<tr><td>Longitude:</td><td>' + markerOverride.getPosition().lng() + '</td></tr>' +
            '<tr><td>Distance from Origin:</td><td>0m</td></tr>' +
            '</table>' +
            '<hr/><span>Drag to alternate point</span>' +
            '</div>',
            maxWidth: 300
        });
        openMarkerInfoWindow(infoWindowOverride, markerOverride);

        rulerpoly = new google.maps.Polyline({
            path: [markerOriginal.position, markerOverride.position] ,
            strokeColor: "#EF3737",
            strokeOpacity: .7,
            strokeWeight: 8
        });
        rulerpoly.setMap(map);

        google.maps.event.addListener(markerOverride, 'drag', function() {
            rulerpoly.setPath([markerOriginal.getPosition(), markerOverride.getPosition()]);
            distanceFromOrigin = getDistanceBetweenMarkers(markerOriginal.getPosition().lat(), markerOriginal.getPosition().lng(), markerOverride.getPosition().lat(), markerOverride.getPosition().lng());
            lblOverrideDistance.innerHTML = '[' + distanceFromOrigin + ' from Origin]';
        });
        google.maps.event.addListener(markerOverride, 'dragend', function(event) { 
            lblOverrideDistance.innerHTML = '';
            infoWindowOverride.setContent('<div class="iw-content">' +
            '<h6>Delivery Point Override:</h6>' +
            '<table>' +
            '<tr><td>Latitude:</td><td>' + markerOverride.getPosition().lat() + '</td></tr>' +
            '<tr><td>Longitude:</td><td>' + markerOverride.getPosition().lng() + '</td></tr>' +
            '<tr><td>Distance from Origin:</td><td>' + distanceFromOrigin + '</td></tr>' +
            '</table>' +
            '</div>');
            openMarkerInfoWindow(infoWindowOverride, markerOverride);
        });
        markerOverride.addListener('click', () => {
            openMarkerInfoWindow(infoWindowOverride, markerOverride);
        });
    }
    else {
        rulerpoly.setMap(null);
        markerOverride.setMap(null);
    }
}


function mapAndReturnAddressData(place) {
    let comparer = [
        {
            google: 'street_number',
            local:  'street_number',
            label:  'Street Number'
        },
        {
            google: 'route',
            local:  'road_name',
            label:  'Road/Street'
        },
        {
            google: 'sublocality_level_1x',
            local:  'suburb',
            label:  'Suburb'
        },
        {
            google: 'locality',
            local:  'town',
            label:  'Town/City'
        },
        {
            google: 'administrative_area_level_2',
            local:  'district',
            label:  'District'
        },
        {
            google: 'administrative_area_level_1',
            local:  'province',
            label:  'Province'
        },
        {
            google: 'postal_code',
            local:  'postal_code',
            label:  'Postal Code'
        }
    ];

    // let result = place.address_components.map(component => ({
    //     field: component.types[0],
    //     value: component.long_name
    // }));

    let results = place.address_components.filter((component) => {
        console.log(comparer.includes(component.types[0]));
    });

    // let resut = place.address_components.filter((item) => {

    // });

    console.log(results);
}



// ==================== UTILITY FUNCTIONS =========================================================
function closeMapOverlay () {
    document.getElementById('map-overlay').classList.remove('active');
}


function toggleAddressSearchTextboxState(disabled) {
    if (disabled) {
        txtAddressSearch.setAttribute('disabled', 'disabled');
        txtAddressSearch.value = '';
        ulPredictions.innerHTML = '';
        changeAutocompleteStatusText('');
    } else {
        txtAddressSearch.removeAttribute('disabled');
        changeAutocompleteStatusText('Type ' + mapConfig.autocomplete.minLength + ' or more characters for results');
        txtAddressSearch.focus();
    }
}


function toggleAddressAvailableForSelection(available) {
    if (available) {
        chkOverrideMarker.removeAttribute('disabled');
        btnAddressAccept.removeAttribute('disabled');
    }
    else {
        chkOverrideMarker.setAttribute('disabled', 'disabled');
        btnAddressAccept.setAttribute('disabled', 'disabled');
    }
}


function changeAutocompleteStatusText(value) {
    addressSearchStatus.innerHTML = value;
}


function toggleElementVisibility(elem, visible) {
    if (elem) {
        if (visible) {
            elem.classList.add('active');
        }
        else {
            elem.classList.remove('active');
        }
    }
}


function openMarkerInfoWindow(whichInfoWindow, whichMarker) {
    if (whichInfoWindow && whichMarker) {
        whichInfoWindow.open(map, whichMarker);
    }
}


/**
 * Credit: https://www.barattalo.it/coding/decimal-degrees-conversion-and-distance-of-two-points-on-google-map/
 */
function getDistanceBetweenMarkers(lat1,lon1,lat2,lon2) {
    let R = 6371;
    let dLat = (lat2-lat1) * Math.PI / 180;
    let dLon = (lon2-lon1) * Math.PI / 180; 
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) * 
        Math.sin(dLon/2) * Math.sin(dLon/2); 
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    let d = R * c;
    if (d > 1) return Math.round(d)+"km";
    else if (d<=1) return Math.round(d*1000)+"m";
    return d;
}