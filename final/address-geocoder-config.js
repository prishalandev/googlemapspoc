const mapConfig = {
    general: {
        activeClassName:            'active',
        showMapButtonText:          'Show Google Map',
    },
    log: {
        predictionResults:          true,
        predictionErrors:           true,
        selectedAddressComponents:  true,
        selectedAddressError:       true,

    },
    api: {
        key:                        'AIzaSyDDhQmSuJYvvn0H45Wy7DA7lZLa-rg70ww',
        callback:                   'initMapService',
    },
    filter: {
        radius:                     5000,
    },
    predictions: {
        keydownDelay:               400,
        returnTypes:                ['address'],
    },
    autocomplete: {
        minLength:                  7,
        requiredFields:             ["address_component", "formatted_address", "geometry.location", "place_id"]
    },
    mapStyling: [
        {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
            {
                "visibility": "off"
            }
            ]
        },
        {
            "featureType": "poi.business",
            "stylers": [
            {
                "visibility": "off"
            }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
            {
                "visibility": "off"
            }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
            {
                "visibility": "off"
            }
            ]
        }        
    ]
};
