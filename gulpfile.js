'use strict';

var gulp 	= require('gulp'),
	sass    = require('gulp-sass'),
	batch	= require('gulp-batch'),
	watch   = require('gulp-watch');
	// concat 	= require('gulp-concat'),
	// rename 	= require('gulp-rename'),
	// uglify 	= require('gulp-uglify');


// -------------------------------------------------------- Transpile sass
gulp.task('sass', function () {
	return gulp.src('./assets/styles/scss/*.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./assets/styles'));
});

gulp.task('sass-new', function() {
	return gulp.src('./new/scss/*.scss')
		.pipe(sass({outputStyle:'expanded'}).on('error', sass.logError))
		.pipe(gulp.dest('./new'));
});

gulp.task('sass-final', function() {
	return gulp.src('./final/src/scss/*.scss')
		.pipe(sass({outputStyle:'expanded'}).on('error', sass.logError))
		.pipe(gulp.dest('./final'));
});

// gulp.task('scripts-final', function() {
// 	return gulp.src('./final/src/scripts/*.js')
// 		.pipe(concat)
// })


// -------------------------------------------------------- Gulp Watch tasks
gulp.task('watch', function () {
	watch('./assets/styles/scss/**/*.scss', batch(function (events, done) {
		gulp.start('sass', done);
	}));

	watch('./new/**/*.scss', batch(function (events, done) {
		gulp.start('sass-new', done);
	}));

	watch('./final/src/**/*.scss', batch(function (events, done) {
		gulp.start('sass-final', done);
	}));
});